class Student{
        private String name,city;
        private int age;
        public Student(String n,String c,int a){
            this.name=n;
            this.city=c;
            this.age=a;
        }
	public int getAge(){
		return this.age;
	}
}
class StudentMain{
    public static void main(String args[]){
        Student s1=new Student("satyansh","shahad",19);
        Student s2=new Student("pavan","mumbai",20);
        Student s3=new Student("om","kalyan",21);
        Student arr[]={s1,s2,s3};
	int total = 0;
	for(int i=0;i<arr.length;i++){
		total += arr[i].getAge();
	}
	int avg=total/arr.length;
	System.out.println(avg);
    }
}

