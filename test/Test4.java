import java.io.*;
class Test4{
        public static void main(String args[]){
        	try{
                	FileInputStream f = new FileInputStream("StudentObject.txt");
               		ObjectInputStream o = new ObjectInputStream(f);
	                Student s = (Student)o.readObject();
			o.close();
                	f.close();
			System.out.println(s.toString());	
        	}
		catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
	        }
	}
}

