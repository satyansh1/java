import java.io.*;
class Student implements Serializable{
	public int age;
	public String name,city;
/*	public int setAge(int a){
                this.age = a;
        }
        public int getAge(){
                return this.age;
        }
*/
	public Student(String n,String c,int a){
		this.age = a;
		this.name = n;
		this.city = c;
	}
	@Override
	public String toString() {
		return "Student:: Name="+this.name+" City="+this.city+" Age="+this.age;
	}

}
class Test3{
	public static void main(String args[]){
	Student s1 = new Student("satyansh","mumbai",19);
	Student s2 = new Student("pavan","vikhroli",21);
	Student s3 = new Student("chirag","ghatkopar",22);
	try{
		FileOutputStream f = new FileOutputStream("StudentObject.txt");
		ObjectOutputStream o = new ObjectOutputStream(f);
		o.writeObject(s1);
		o.writeObject(s2);
		o.writeObject(s3);
		f.close();
	
	}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
