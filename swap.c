#include<stdio.h>
void swapp(int *a,int *b){	//call by reference function
	*a=*a+*b;
        *b=*a-*b;
        *a=*a-*b;
}
/*void swapp(int a,int b){      call by value function
        a=a+b;
        b=a-b;
        a=a-b;
}*/
int main(){
	int x=10,y=20;
	printf("before swapping value of x is %d\n",x);
	printf("before swapping value of y is %d\n",y);
	swapp(&x,&y);	//call by reference
	//swapp(x,y); 	//call by value
	printf("after swapping value of x is %d\n",x);
	printf("after swapping value of y is %d\n",y);
	return 0;

}
