import java.util.Scanner;
class If_14{
        public static void main(String args[]){
            Scanner sc=new Scanner(System.in);
		
			System.out.print("Enter the salary : ");
			int salary=sc.nextInt();
			float commision;

			if(salary<10000){
				commision=salary*0.1f;
				System.out.println("10% commision of given amount of salary will be:  "+commision);	
			}
			else if(salary>=10000&&salary<=20000){
				commision=salary*0.12f;
				System.out.println("12% commision of given amount of salary will be:  "+commision);
			}
			else if(salary>20000){
				commision=salary*0.15f;
				System.out.println("15% commision of given amount of salary will be:  "+commision);
			}
			else{
				System.out.println("No commision");
			}
			}
		}