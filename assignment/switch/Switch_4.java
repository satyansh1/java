/*WAP to read a color code (char value) and print appropriate color.
(e.g. R – Red, G- Green, B-Blue and other char – Black)*/


import java.util.Scanner;
class Switch_4{
        public static void main(String ar[]){
        Scanner sc=new Scanner(System.in);

        System.out.println("Enter the color: ");
        char color=sc.next().charAt(0);
        

        switch(color){
                case 'r':
                        System.out.println("RED");
                        break;
                case 'g':
                        System.out.println("GREEN");
                        break;
                case 'b':
                        System.out.println("BLUE");
                        break;
                default:
                        System.out.println("BLACK");

        }
        }
}

