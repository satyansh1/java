class Database{
	static Database obj=new Database();
	private Database(){
		System.out.println("Database object initialized !!");
	}
	public static Database getConnectionObject(){
		System.out.println("Connection object returned!");
		return obj;
	}
}
class Mainsingleton{
	public static void main(String args[]){
		Database d1 = Database.getConnectionObject();
		Database d2 = Database.getConnectionObject();
	}
}
