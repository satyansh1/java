class Database {
	static Database obj;
	private Database(){
		System.out.println("Database object initialized !!");
	}
	public static Database getConnectionObject(){
		synchronized(Database.class){	     //using Database.class instead of this bcz method is static
			if(obj==null){
				obj = new Database();
			}
			System.out.println("Connection object returned...");
		}
			return obj;
	}
}
enum Mydb{
	INSTANCE;
	int bal;
	public void getConn(){
		System.out.println("my db instance created  "+this.bal);
	}
}
class Mainsingleton1{
	public static void main(String args[]){
		
		Thread t1 = new Thread(()->
                        {
                                Database d1 = Database.getConnectionObject();                                     }
                );
                Thread t2 = new Thread(()->
                        {
                                 Database d2 = Database.getConnectionObject();
                        }
                );
                t1.start();
                t2.start();
		Mydb m1 = Mydb.INSTANCE;
		Mydb m2 = Mydb.INSTANCE;
		m1.bal = 1000;
		m1.getConn();	
		m2.getConn();	
	}
}

