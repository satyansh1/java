import java.util.Scanner;
class Calculate{
	int c=0;
	public Calculate(){
		System.out.println("Calculation begins!!");
	}	
	public void cal(){
		System.out.println("Cal method!!");
	}
}
class Sum extends Calculate{
	int a,b;
	public Sum(int x,int y){
		this.a=x;
		this.b=y;	
	}
	public void cal(){
		c=this.a+this.b;
		System.out.println("Sum is :"+c);
	}
}
class Sub extends Calculate{
	int c,d;
	public Sub(int p,int q){
		this.c=p;
		this.d=q;
	}
	public void cal(){
		c=this.c-this.d;
        	System.out.println("Substraction is :"+c);
	}
}
class Mul extends Calculate{
	int e,f;
	public Mul(int l,int m){
		this.e=l;
		this.f=m;
	}
	public void cal(){
		c=this.e*this.f;
        	System.out.println("product is :"+c);
	}
}
class Div extends Calculate{
	int k,t;
	public Div(int g,int h){
		this.k=g;
		this.t=h;
	}
	public void cal(){
		c=this.k/this.t;
        	System.out.println("Division is :"+c);
	}
}
class Main7{
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your choice :");
		int choice = sc.nextInt();
		Calculate c1;
		switch(choice){
			case 1:
				c1=new Sum(4,5);
				break;
			case 2:
				c1=new Sub(8,9);
				break;
			case 3:
				c1=new Mul(2,6);
				break;
			case 4:
				c1=new Div(4,2);
				break;
			default:
				c1=new Calculate();
		}
		c1.cal();
	}
}
