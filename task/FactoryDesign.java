import java.util.Scanner;
interface Mydb{
	public void select();
	//public void insert();
	//public void delete();
	//public void update();
}
class Mysqldb implements Mydb{
	public void select(){
		System.out.println("I am mySQL select");
	}
}
class Oracledb implements Mydb{
	public void select(){
		System.out.println("I am Oracle select");
	}
}  
class DependencyInjection{
	Mydb dbobj;
	public DependencyInjection(String db){
			switch(db){
				case "MySQL":
						this.dbobj = new Mysqldb();
						break;
				case "Oracle":
						this.dbobj = new Oracledb();
						break;
				default :
						this.dbobj = new Mysqldb();
						//System.out.println("Invalid Choice!!");
			}
	}
	public void select(){
		this.dbobj.select();
	}
}
class FactoryDesignMain{
	public static void main(String args[]){
			System.out.println("Enter your choice: ");
			Scanner sc = new Scanner(System.in);
			String s=sc.nextLine();
			DependencyInjection di = new DependencyInjection(s);
			di.select();
	}
}
