class RBI{
	private float bal;
	public float cb=0;
	public RBI(float b){
	this.bal=b;
	System.out.println("\nBALANCE : "+this.bal);
	}
	public void deposit(float damt){
		System.out.println("\nENTER THE AMOUNT TO BE DEPOSITED : "+damt);
		cb=damt+this.bal;
	}
	public void withdraw(float wamt){
		System.out.println("ENTER THE AMOUNT TO BE WITHDRAWN : "+wamt);
		cb=this.bal-wamt;
	}
	public void showBal(){
		System.out.println("\nYOUR CURRENT BALANCE IS : "+cb+"\n");
		this.bal=cb;
		}
}
class RBImain{
	public static void main(String args[]){
		RBI acc1=new RBI(500f);
		acc1.deposit(200f);
		acc1.showBal();
		acc1.withdraw(150f);
		acc1.showBal();
	}
}
