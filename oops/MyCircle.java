class MyCircle{
	private int radius; 
		public MyCircle(){
			this.radius = 1;
			System.out.println("Constructor called");
	}	
		public MyCircle(int r){
			this.radius = r;
			System.out.println("parameterized Constructor called");
		}
		public void area(){
			System.out.println("area of circle is"+(3.14f*this.radius*this.radius));
		}
		public void circumference(){
			
			System.out.println("circumference of circle is"+(2*3.14f*this.radius));
		}	

		@Override												//overriding tostring method
		public String toString(){
			return "current state of radius is:"+this.radius;
		}
}
class MainClass{
	public static void main(String a[]){
		MyCircle c1=new MyCircle();
		MyCircle c2=new MyCircle(5);
		c1.area();
		c2.area();
		c1.circumference();
		c2.circumference();
		System.out.println(c1);//tostring method called
		System.out.println(c2);//tostring method called

			}
}
