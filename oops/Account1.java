import java.util.Scanner;
abstract class RBI{
	Scanner sc=new Scanner(System.in);
	protected int balance;
	public RBI(int b){
		this.balance=b;
	}
	abstract public void deposit();
	abstract public void withDraw();
	abstract public void showBalance();
}
class SBI extends RBI{
	public SBI(int bal){
		super(bal);
		System.out.println("your current balance is :"+this.balance);
	}
	public void deposit(){
		System.out.println("Enter the amount to be deposited :");
		int d = sc.nextInt();
		this.balance+=d;
	}
	public void withDraw(){
		for(int c=0;c<=4;c++){
			if(c<3){
				System.out.println("Enter the amount to be withdrawn :");
                		int w = sc.nextInt();
               			this.balance=this.balance-w;
			}
			else{
				System.out.println("Enter the amount to be withdrawn :");
                                int w = sc.nextInt();
                                this.balance=(this.balance-w)-25;
				}
					c++;
			}
		}	
	public void showBalance(){
		System.out.println("your current balance is :"+this.balance);
	}
}
class HDFC extends RBI{
	public HDFC(int b){
		super(b);
		System.out.println("your current balance is :"+this.balance);
	}
	public void deposit(){
		System.out.println("Enter the amount to be deposited :");
		int d = sc.nextInt();
		this.balance+=d;
	}
	public void withDraw(){
		for(int c=0;c<=4;c++){
			if(c<3){
				System.out.println("Enter the amount to be withdrawn :");
                		int w = sc.nextInt();
               			this.balance=this.balance-w;
			}
			else{
				System.out.println("Enter the amount to be withdrawn :");
                                int w = sc.nextInt();
                                this.balance=(this.balance-w)-30;
				}
					c++;
			}
		}	
	public void showBalance(){
		System.out.println("your current balance is :"+this.balance);
	}
}
class MainClass{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number to choose bank: ");
		int a=sc.nextInt();
		RBI r1=new SBI(200);
		switch(a){
			case 1:
				System.out.println("WELCOME TO SBI : ");
				System.out.println("1.Deposit\n2.Withdraw\n3.showbalance");
				int b=sc.nextInt();
				if(b==1){
					r1.deposit();
				}
				if(b==2){
					r1.withDraw();
					r1.showBalance();
				}
				else{
					r1.showBalance();
				}
				break;
			case 2:
				RBI r2=new HDFC(300);
				System.out.println("WELCOME TO HDFC : ");
				System.out.println("1.Deposit\n2.Withdraw\n3.showbalance");
				int c=sc.nextInt();
				if(c==1){
					r2.deposit();
				}
				if(c==2){
					r2.withDraw();
					r2.showBalance();
				}
				else{
					r2.showBalance();
				}
				break;
			default:
				System.out.println("INVALID CHOICE!!! ");
			}
		}
	}

