import java.util.Scanner;
import java.lang.Thread;
class Bank{
	public int bal=0;
	Scanner sc = new Scanner(System.in);
	public Bank(int amt){
		this.bal=amt;
		System.out.println("");
		System.out.println("Your current balance is :"+this.bal);
	}
	public synchronized void deposit(int d){
		this.bal=this.bal+d;
		System.out.println("After depositing amount ------"+d+"------ your current balance is :"+this.bal);
		notify();
	}
	public synchronized void withdraw(int wd){
		if(wd>this.bal){
			System.out.println("Insufficient balance!!!");
			try{
			wait();
			}
			catch(Exception e){};
		}
		
			this.bal=this.bal-wd;
			System.out.println("After withdrawing amount ------" +wd+ "------ Your current balance is :"+this.bal+"\n");
	
	}
}
class MainBank{
	public static void main(String args[]){
		Bank t1 = new Bank(3000);
		Thread wt = new Thread(()->
				{
					t1.withdraw(5000);			
				}
		);
		Thread dt = new Thread(()->
				{
					t1.deposit(2000);
				}
		);
		dt.start();
		wt.start();
	}
}
