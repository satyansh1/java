import java.util.Scanner;
class Account extends Thread{
	int balance=0;
	Scanner sc = new Scanner(System.in);
	public Account(int amt){
		this.balance=amt;
		System.out.println("Your current balance is :"+this.balance);
	}
	public synchronized void deposit(){
		System.out.println("Enter the amount to be deposit :");
		int d = sc.nextInt();
		this.balance=this.balance+d;
		System.out.println("Your current balance after depositing is :"+this.balance);
	}
	public synchronized void withdraw(){
		System.out.println("Enter the amount to be withdraw :");
        int wd = sc.nextInt();
        if(wd>this.balance){	
		System.out.println("Insufficient balance!!!");
		try{Thread.sleep(1000);}catch(Exception e){}
		}
	else{
		this.balance=this.balance-wd;
	        System.out.println("Your current balance after withdrawl is :"+this.balance);
	}	
	}
}
class MainAccount{
	public static void main(String args[]){
		Account a1 = new Account(500);	
		Thread wt = new Thread(()->{
			a1.withdraw();
		}				
		);
		Thread dt = new Thread(()->{
			a1.deposit();
		}
		);
		dt.start();
		wt.start();
	}
}
