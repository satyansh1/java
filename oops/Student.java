class Student{
	
		private String name,city;
		private int age;
		

	public Student(String n,int a,String c){
		this.name=n;
		this.age=a;
		this.city=c;
	}
	void showData(){
		System.out.println("name is:"+name);
		System.out.println("age is:"+age);
		System.out.println("city is:"+city);
	}
	@Override
	public String toString(){											//tostring method
				return this.name+"|\t"+this.age+"|\t"+this.city+"\n";
	}
	@Override
	public int hashCode(){												//hashcode method
		return this.age + this.name.hashCode() + this.city.hashCode();

	}

	public int getAge() {					//defining methods 
			return this.age;
	}

	public String getName() {
		return this.name;
	}

	public String getCity() {
		return this.city;
	}
	
	@Override							//implementing equals method
	public boolean equals(Object kuchbhi){
			if(kuchbhi == this) {
					return true;
			}

			if(kuchbhi instanceof Student) {
					Student localstudent = (Student) kuchbhi;
					return (localstudent.getAge() == this.age ) && (localstudent.getName().equals(this.name)) && (localstudent.getCity().equals(this.city));
			}

			return false;

	}
	public void setName(String ln){
		this.name=ln;
	}
}


class MainClass {
	public static void main(String args[]){
		Student s=new Student("satyansh",19,"mumbai");
		Student s1=new Student("om",20,"shahad");
		Student s2=new Student("satyansh",19,"mumbai");

	//	s.showData();
	//	s1.showData();

		String name="sattu";
		//s.age=150;	
		System.out.println("name|\t\tage|\tcity\n");
		//System.out.println(s.hashCode());
		//System.out.println(s1.hashCode());
		System.out.println(s);
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s.equals(s2)); 
		System.out.println("hashcode of s is:"+s.hashCode());
		System.out.print("hashcode of s2 is:"+s2.hashCode());
		s.setName("sattu");
		System.out.println("hashcode of s is:"+s.hashCode());
		//System.out.println();
		
	}

}
