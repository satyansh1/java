class Employee{
	private int age;
	private String name,city;
	private float salary;
		public Employee(){
			System.out.println("Default constructor called:");
		}		
		public Employee(int a,String n,String c,float s){
			this.age=a;
			this.name=n;
			this.city=c;
			this.salary=s;
			System.out.println("parameterized constructor called!!");
		}
		@Override
		public int hashCode(){
			System.out.println("HashCode called!!");
			return this.age+this.name.hashCode()+this.city.hashCode()+Float.floatToIntBits(this.salary);
			
		}
		@Override
		public boolean equals(Object obj){
/*			if(this.hashCode()==obj.hashCode()){
				return true;
			}
			else{*/
				if(obj instanceof Employee){
					Employee e=(Employee)obj;
						if(this.age==e.age&&this.name.equals(e.name)&&
							this.city.equals(e.city)&&this.salary==e.salary){
							return true;
							}
			}
			return false;
			
			}
}
class MainClass{
		public static void main(String args[]){
	Employee e1 = new Employee(19,"satyansh","shahad",25000);
	Employee e2 = new Employee(19,"satyansh","shahad",20000);
	Employee e3 = new Employee(19,"satyansh","shahad",20000);
	System.out.println(e1.hashCode());
	System.out.println(e2.hashCode());
	System.out.println(e2.equals(e3));
	}
}

