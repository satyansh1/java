class Overload{
	int sum=0;
	public Overload(){
		System.out.println("Constructor called!!");
	}
	public void add(int i){
		System.out.println("First method");
		sum+=i;
		System.out.println(sum);
	}
	public void add(int i,int j){
		sum=i+j;
		System.out.println("SECOND METHOD");
		System.out.println(sum);
	}
	public void add(float i,int j){
		float sum=0;
		sum=i+j;
		System.out.println("THIRD METHOD");
                System.out.println(sum);
	}

	public static void main(String args[]){
		Overload o=new Overload();
		o.add(9);	
		o.add(4,51);
		o.add(12f,5);
	}
}
