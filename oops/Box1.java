class Box1{
	private int length,breadth,height;
	public Box1(){
		this.length=1;
        this.breadth=1;
        this.height=1;
	}
	public Box1(int l){
		this.length=l;
	}	
	Box1(int l,int b){
		this.length=l;
		this.breadth=b;
	}
	Box1(int l,int b,int h){
		this.length=l;
        this.breadth=b;
		this.height=h;
	}
	public void volume(){
		int v=0;
		v=this.length*this.breadth*this.height;
		System.out.println("volume of box is : "+v);
	}
}
class Box1main{
	public static void main(String args[]){
		Box1 b1=new Box1(5);
		Box1 b2=new Box1(5,6);
		Box1 b3=new Box1(5,6,7);
		Box1 b4=new Box1();
		b1.volume();
		b2.volume();
		b3.volume();
		b4.volume();
	}

