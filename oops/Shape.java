import java.util.Scanner;
class Shape{
        public Shape(){
                System.out.println("THIS IS SHAPE!!");
        }
        public void area(){
                System.out.println("THIS IS AREA METHOD OF SHAPE CLASS!!!");
        }
}
class Circle extends Shape{
        int radius;
        public Circle(int r){
                this.radius=r;
                System.out.println("CIRCLE DRAWN!!");
        }
        public void area(){
                System.out.println("RADIUS IS : "+this.radius);
        }
}
class Rectangle extends Shape{
        int l,b;
        Rectangle(int length,int breadth){
                this.l=length;
                this.b=breadth;
                System.out.println("RECTANGLE DRAWN!!");
        }
        public void area(){
                int area=this.l*this.b;
                System.out.println("AREA OF RECTANGLE IS: "+area);
        }
}
class ShapeMain{
        public static void main(String args[]){
        	Scanner sc=new Scanner(System.in);
	 	System.out.println("Enter the choice:");	
	        int a=sc.nextInt();
		Shape s;
		switch(a){
			case 1:
				s=new Circle(5);
				break;
			case 2:
        		        s=new Rectangle(4,5);
				break;
			default:
				s=new Shape();
		}
		s.area();
        }
}

