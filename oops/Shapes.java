class Shape{
	public Shape(){
		System.out.println("THIS IS SHAPE!!");
	}
	public void area(){
		System.out.println("THIS IS AREA METHOD OF SHAPE CLASS!!!");
	}
}
class Circle extends Shape{
	int radius;
	public Circle(int r){
		this.radius=r;
		System.out.println("THIS IS CIRCLE!!");
	}
	public void area(){
		System.out.println("RADIUS IS : "+this.radius);
	}
}
class Rectangle extends Shape{
	int l,b;
	Rectangle(int length,int breadth){
		this.l=length;
		this.b=breadth;
		System.out.println("RECTANGLE CONSTRUCTOR CALLED!!");
	}
	public void area(){
		int area=this.l*this.b;
		System.out.println("AREA OF RECTANGLE IS: "+area);
	}
}
class ShapeMain{
	public static void main(String args[]){
		Circle c=new Circle(5);
		c.area();
		Rectangle r=new Rectangle(4,5);
		r.area();		
	}
}
