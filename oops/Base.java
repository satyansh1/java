class Base{
	public Base(){
		System.out.println(" BASE constructor called !!");
	}
}
class Derived extends Base{
	public Derived(){
		System.out.println(" DERIVED constructor called !!");
	}
}
class BaseMain{
	public static void main(String args[]){
//	Base b=new Base();
	new Derived();
	}
}
