class Set{
	int a,b,c;
	public Set(int num1,int num2,int num3){
		this.a=num1;
		this.b=num2;
		this.c=num3;
	}
	public void sum(){
		int sum=0;
		sum=this.a+this.b+this.c;
		System.out.println("sum is:"+sum);
	}
	public void mean(){
		int m=this.a+this.b+this.c/3;
		System.out.println("mean is:"+m);
	}
	public void max(){
		if(this.a>this.b&&this.a>this.c){
			System.out.println("maximum number is:"+this.a);
		}
		else if(this.b>this.a&&this.b>this.c){
			System.out.println("maximum number is:"+this.b);
		}
		else{
			System.out.println("maximum number is:"+this.c);	
		}
	}
	public void min(){
		if(this.a<this.b&&this.a<this.c){
			System.out.println("minimum number is:"+this.a);
		}
		else if(this.b<this.a&&this.b<this.c){
			System.out.println("minimum number is:"+this.b);
		}
		else{
			System.out.println("minimum number is:"+this.c);	
		}
	}
}
class SetMain{
	public static void main(String[] args) {
		Set s=new Set(4,2,8);
		s.sum();
		s.mean();
		s.max();
		s.min();
	}
}