interface RBI{
	public void withdraw(int amt);
	public void deposit(int amt);	
}
class SBI implements RBI{
	int balance;
	public void deposit(int amt){
		System.out.println("enter the amount to be deposited: "+amt);
		this.balance+=amt;
		System.out.println(this.balance);
	}
	public void withdraw(int amt){
		System.out.println("enter the amount to be withdrawn: "+amt);
                this.balance-=amt;
		System.out.println(this.balance);
	}

}
class Main1{
	public static void main(String args[]){
		SBI s1=new SBI();
		s1.deposit(300);
		s1.withdraw(100);
	}
}
