import java.util.*;
class Student implements Comparable{
	public int age,rollno;
	public String name,city;
	public Student(String n,String c,int a,int rn){
			this.name = n;
			this.city = c;
			this.age = a;
			this.rollno = rn;
	}	
	public int compareTo(Object o){
		Student s = (Student)o;
		if (this.rollno>s.rollno) {
			return 1;
		}
		else{
			return -1;
		}
	}
	public String toString(){
		return this.name+" | "+this.city+" | "+this.age+" | "+this.rollno;
	}
}
class Maincomp{
	public static void main(String[] args) {
		Student obj1 = new Student("satyansh","shahad",19,69);
		Student obj2 = new Student("pavan","vidyavihar",18,115);
		Student obj3 = new Student("chirag","thane",20,31);
		
		List<Student> stud = new ArrayList<>();
		stud.add(obj1);
		stud.add(obj2);
		stud.add(obj3);

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter which data you want to sort:");
		int a = sc.nextInt();
		switch (a) {
			case 1: 
					Collections.sort(stud,(o1,o2)->
					{
						Student s1 = (Student)o1;
						Student s2 = (Student)o2;
						return s1.name.compareTo(s2.name);
					}
					);
					System.out.println("==========\nAfter sorting Name\n==========\n");
					stud.forEach(System.out::println);
					break;
			case 2: 
					Collections.sort(stud,(o3,o4)->
					{
						Student s1 = (Student)o3;
						Student s2 = (Student)o4;
						return s1.city.compareTo(s2.city);
					}
					);
					System.out.println("==========\nAfter sorting city\n==========\n");
					stud.forEach(System.out::println);
					break;
			case 3:
					Collections.sort(stud,(o5,o6)->
					{
						Student s1 = (Student)o5;
						Student s2 = (Student)o6;
						return s1.age > s2.age ? 1 :-1;
					}
					);
					System.out.println("==========\nAfter sorting Age\n==========\n");
					stud.forEach(System.out::println);
			default:
					Collections.sort(stud);
					System.out.println("==========\nAfter sorting RollNo\n==========\n");
					stud.forEach(System.out::println);
					break;					
		}
	}
}
