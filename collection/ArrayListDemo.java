import java.util.*;
class ArrayListDemo{
	public static void main(String args[]){
		List<Integer> fibonacci = new ArrayList<>();
		fibonacci.add(0);
		fibonacci.add(2);
		fibonacci.add(1);
		fibonacci.add(1);
		fibonacci.add(8);
		fibonacci.add(5);
		fibonacci.add(2,3);
		fibonacci.add(6);
		fibonacci.remove(fibonacci.indexOf(6));    //removing element without knowing index

		System.out.println("Before sorting: ");
		fibonacci.forEach(System.out::println);

		Collections.sort(fibonacci);
		System.out.println("After sorting: ");
		for(int i=0;i<fibonacci.size();i++){ 	   //iterating using size method
			System.out.println(i);
		}
		System.out.println("Size of Arraylist is :"+fibonacci.size());

	}
}
	
