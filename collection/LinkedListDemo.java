import java.util.*;
class LinkedListDemo{
	public static void main(String args[]){
		List<String> days = new LinkedList<>();
		days.add("tue");
		days.add("thur");
		days.add("wed");
		days.add("sat");
		days.add("sun");
		days.add("fri");
		days.add("mon");

		days.remove(days.indexOf("sun"));
		System.out.println("Before sorting :");
		for(String i:days){
			System.out.println(i);
		}
		System.out.println("After sorting :");
		Collections.sort(days);
		for(String j:days){
			System.out.println(j);
		}
		System.out.println("Size of linked list is:"+days.size());


	}
}
