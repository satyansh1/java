import java.util.*;
class MapDemo{
	public static void main(String args[]){
		Map fruit = new HashMap();
		fruit.put("Nagpur","Orange");
		fruit.put("Nasik","Grapes");
		fruit.put("Jalgaon","Banana");
		fruit.put("Ratnagiri","Mango");
		fruit.put("Kashmir","Apple");
		fruit.put(null,"Aloo");
		Set set = fruit.entrySet();
		
		Iterator i = set.iterator();
		while(i.hasNext()){
			Map.Entry e = (Map.Entry)i.next();
			System.out.println(e.getKey()+"   "+e.getValue());
		}
		
		System.out.println("--------------------");
		System.out.println(fruit.get("Nasik"));
		System.out.println("--------------------");
		Map fruits = new LinkedHashMap();
                fruits.put("Nagpur","Orange");
                fruits.put("Nasik","Grapes");
                fruits.put("Jalgaon","Banana");
                fruits.put("Ratnagiri","Mango");
                fruits.put("Kashmir","Apple");
                fruits.put(null,"Aloo");
                Set s = fruits.entrySet();

                Iterator itr = s.iterator();
                while(itr.hasNext()){
                        Map.Entry e = (Map.Entry)itr.next();
                        System.out.println(e.getKey()+"   "+e.getValue());
                }
		
		System.out.println("--------------------");	//only keys get sorted
                Map fruit1 = new TreeMap();	//sorting:1)capital letters then small letters
                fruit1.put("Nagpur","Orange");
                fruit1.put("Nasik","Grapes");
                fruit1.put("Jalgaon","Banana");
                fruit1.put("Ratnagiri","Mango");
                fruit1.put("Kashmir","Apple");
                fruit1.put("Null","Aloo");
                Set s1 = fruit1.entrySet();

                Iterator it = s1.iterator();
                while(it.hasNext()){
                        Map.Entry e = (Map.Entry)it.next();
                        System.out.println(e.getKey()+"   "+e.getValue());
                }
		System.out.println("--------------------");
		Hashtable fruit2 = new Hashtable();
                fruit2.put("Nagpur","Orange");
                fruit2.put("Nasik","Grapes");
                fruit2.put("Jalgaon","Banana");
                fruit2.put("Ratnagiri","Mango");
                fruit2.put("Kashmir","Apple");
                fruit2.put("Null","Aloo");
                Set set1 = fruit2.entrySet();

                Iterator i1 = set1.iterator();
                while(i1.hasNext()){
                        Map.Entry e = (Map.Entry)i1.next();
                        System.out.println(e.getKey()+"   "+e.getValue());
                }

	}
}
