import java.util.*;
class Maincollection{
	public static void main(String args[]){
		
		//ArrayList
		Collection fruits = new ArrayList();
		fruits.add("mango");
		fruits.add("orange");
		fruits.add("apple");
		fruits.add("banana");
		//System.out.println("FRUITS ARE::"+fruits);
		for(Object i:fruits){
			System.out.println(i);
		}
		fruits.forEach((x)->{		//foreach loop with lamda expression 
			System.out.println(x);
		}
		);

		//HashSet
		Collection weekdays = new HashSet();
		weekdays.add("monday");
		weekdays.add("tuesday");
		weekdays.add("wednesday");
		weekdays.add("thursday");
		weekdays.add("friday");
		weekdays.add("saturday");
		weekdays.add("sunday");
		//System.out.println("Days in a week are ::"+weekdays);
		weekdays.forEach((j)->{
			System.out.println(j);
		});

		//HashMap
		Map fruit = new HashMap();
		fruit.put("Nagpur","orange");
		fruit.put("jalgaon","banana");
		fruit.put("nashik","grapes");
		fruit.put("ratnagiri","mango");
		//System.out.println("\nFruits are --"+fruit);

	}
}
