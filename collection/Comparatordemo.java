import java.util.*;

class AgeComparator implements Comparator {
	public int compare(Object o1, Object o2) {
		Student s1 = (Student) o1;
		Student s2 = (Student) o2;
		return s1.age > s2.age ? 1 : -1;
	}
}
/*class NameComparator implements Comparator{ 
        public int compare(Object o3,Object o4)     {
                Student s1 = (Student) o3;
                Student s2 = (Student) o4;
                return s1.name.compareTo(s2.name);
        }
}
class CityComparator implements Comparator{
        public int compare(Object o5,Object o6) {
                Student s1 = (Student) o5;
                Student s2 = (Student) o6;
                return s1.city.compareTo(s2.city);
        }
}*/
class Student implements Comparable {
	String name,city;
	int age,rollno;
	public int compareTo(Object o){
		Student s = (Student) o;
		if(this.rollno>s.rollno)
			return 1;
		else
			return -1;
	}
	public Student(String n1,int a1, String c1,int r1 ){
		this.name = n1;
		this.age = a1;
		this.city = c1;
		this.rollno = r1;
	}
	public String toString() {
		return "Name: "+this.name+"|   Age: "+this.age+"|   City: "+this.city+"|   Roll No: "+this.rollno;
	}
}

class StudentMain{
	public static void main(String[] arg){
		Student sobj1 = new Student("Satyansh",18,"Mumbai ",69);
		Student sobj2 = new Student("Pavan   ",20,"ghatkopar ",117);
		Student sobj3 = new Student("Chirag  ",19,"Vikhroli",31);
		List<Student> student = new ArrayList<>();

		student.add(sobj1);
		student.add(sobj2);
		student.add(sobj3);

		student.forEach(System.out::println);
		Collections.sort(student);
		System.out.println("\n=============\nAfter Sorting Roll No\n=============\n");
		student.forEach(System.out::println);
		Collections.sort(student, new NameComparator(){
			public int compare(Object o3,Object o4)     {
                		Student s1 = (Student) o3;
	                	Student s2 = (Student) o4;
	        	        return s1.name.compareTo(s2.name);
		        }
		}
		);
		System.out.println("\n=============\nAfter Sorting Name\n=============\n");
		student.forEach(System.out::println);

		Collections.sort(student, new AgeComparator());
                System.out.println("\n============\nAfter Sorting Age\n============\n");
                student.forEach(System.out::println);

		Collections.sort(student,(o5,o6)->{
			
        		        Student s1 = (Student) o5;
        		        Student s2 = (Student) o6;
        		        return s1.city.compareTo(s2.city);
		});
		
                System.out.println("\n=============\nAfter Sorting City\n=============\n");
                student.forEach(System.out::println);

	}
}

