import java.util.*;
class IteratorDemo{
	public static void main(String args[]){
		Collection<String> fruit = new ArrayList<>();
		fruit.add("Orange");
		fruit.add("Grapes");
		fruit.add("Banana");
		fruit.add("Mango");
		fruit.add("Apple");

		Iterator i = fruit.iterator();
		while(i.hasNext()){
			System.out.println(i.next());
		}
	}	
}
