import java.util.*;
class StackDemo{
        public static void main(String args[]){
                Stack<Integer> days = new Stack<>();
                days.push(2);
                days.push(3);
                days.push(6);
                days.push(5);
                days.push(7);
                days.push(1);
                days.push(4);

                days.pop();
                System.out.println("Before sorting :");
                for(int i:days){
                        System.out.println(i);
                }
                System.out.println("After sorting :");
                Collections.sort(days);
                days.pop();
                for(int j:days){
                        System.out.println(j);
                }
                System.out.println("Size of Stack is:"+days.size());


        }
}
