import java.util.*;
class Weekdays{
	public static void main(String args[]){
		Set<String> wd = new HashSet<>();
		wd.add("Mon");
		wd.add("Tue");
		wd.add("Wed");
		wd.add("Thur");
		wd.add("Fri");
		wd.add("Sat");
		wd.add("Sun");
		wd.forEach(System.out::println);
		System.out.println("---------After sorting Hashset---------");
		TreeSet<String> sortedwd = new TreeSet<>();
		sortedwd.addAll(wd);
		for(String i:sortedwd){
			System.out.println(i);
		}
		System.out.println("---------LinkedHAshSet---------");

                Set<String> wd1 = new LinkedHashSet<>();
                wd1.add("Mon");
                wd1.add("Tue");
                wd1.add("Wed");
                wd1.add("Thur");
                wd1.add("Fri");
                wd1.add("Sat");
                wd1.add("Sun");
                wd1.forEach(System.out::println);
		
		Set<Integer> fibonacci = new TreeSet<>();
		fibonacci.add(3);
		fibonacci.add(0);
		fibonacci.add(1);
		fibonacci.add(2);
		fibonacci.add(5);
		fibonacci.add(1);

		for(int i:fibonacci){
			System.out.println(i);
		}
	}
}


